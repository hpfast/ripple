import React, { Component } from 'react';
import './App.css';

function fillArray(start, end) {
  return Array(end - start).fill().map((_, idx) => start + idx)
}

function cellDistance(origin, target) {
  let x = Math.abs(target.col - origin.col);
  let y = Math.abs(target.row - origin.row);
  let distance = Math.round(Math.hypot(x,y));
  return distance
}

class App extends Component {
  constructor() {
    super()
    this.state = {
      clickedCell: null,
      color: 'color1'
    }
  }
  updateDimensions() {
    let cellSizePx = Math.floor(window.innerWidth / 20);
    let gridWidth = 19;
    let gridHeight = Math.floor(window.innerHeight / cellSizePx);
    let numCells = fillArray(0, Math.floor(gridWidth * gridHeight));
    this.setState({cellSizePx, gridWidth, gridHeight,  numCells})
  }

  componentWillMount() {
    this.updateDimensions();
  }
  
  componentDidMount() {
    window.addEventListener('resize', this.updateDimensions.bind(this));
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateDimensions);
  }

  handleClick(clickedCell) {
    let color = (this.state.color === 'color1' ? 'color2' : 'color1')
    this.setState({clickedCell, color})
  }

  render() {
    return (
      <div className="App">
        <RippleGrid handleClick={this.handleClick.bind(this)} {...this.state}/>
      </div>
    );
  }
}


class RippleGrid extends Component {
  rowCol(clicked){
    return {
      row: Math.ceil(clicked/this.props.gridWidth),
      col: clicked % this.props.gridWidth
    }
  }

  createCells(cells) {
    return cells.map(this.createCell.bind(this)) 
  }

  createCell(i){
    let props = Object.assign({}, this.props, {key: i, id: i});
    if (props.clickedCell !== null) {
      let origin = this.rowCol(props.clickedCell);
      let target = this.rowCol(i);
      props.distance = cellDistance(origin, target);
    }
    return <Cell handleClick={this.props.handleClick} {...props}/>
  }

  render() {
    return (
      <div className="ripple-grid">
      {
        this.createCells(this.props.numCells)
      }
      </div>
    )
  }
}

class Cell extends Component{
  getStyle(){
    return {
      width: Math.floor(this.props.cellSizePx),
      height: Math.floor(this.props.cellSizePx),
      transition: `background-color ${this.props.distance}00ms step-end`
    }
  }

  _onClick() {
    this.props.handleClick(this.props.id)
  }

  render(){
   return (
     <div className={`cell ${this.props.color}`} onClick={this._onClick.bind(this)} style={this.getStyle()}/>
     );
  }
}

export default App;
