Ripple Grid
===========

A simple React app made to explore a 'ripple effect' animation. The app consists of a grid of cells. When one is clicked, the cells change colour in a circular ripple pattern originating from the clicked cell.
